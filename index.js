var open = false;
var Store = [];
var count = 0;
var called;
var Extend = function(Klass, ErrorKlass) {
	ErrorKlass = ErrorKlass || Object;

	ErrorKlass = ErrorKlass || Error;
	Klass.prototype = Object.create(ErrorKlass.prototype);
	Klass.prototype.constructor = Klass;

	Klass.prototype.super = (function(ErrorKlass) {
			return function() {
				var self = this;
				var _super = this.super;
				if(ErrorKlass.prototype.super){
					this.super = ErrorKlass.prototype.super;
				}
				ErrorKlass.apply(this,arguments);
				this.super = _super;
			}
		} )(ErrorKlass)

	Klass.prototype.superArgs = (function(ErrorKlass) {
		return function(args) {
			var _superArgs = this.superArgs;
			this.superArgs = ErrorKlass.prototype.superArgs || function() {};
			var self = this;
			ErrorKlass.apply(this, args);
			this.superArgs = _superArgs;
		}
	})(ErrorKlass)

	Store[Klass.name] = (function(Klass, ErrorKlass) {
		return Klass;
	})(Klass, ErrorKlass);
}


module.exports = Extend;